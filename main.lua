function love.conf(t)
	t.title = "Car"
end

function love.load()
	love.filesystem.load("AnAL.lua")()
	w = 300
	h = 700
	u = 70	-- meter

	world = love.physics.newWorld(w, h)
	world:setGravity(0, 0)
	world:setMeter(u)

	love.graphics.setMode(w, h, false, true, 0)

	i_car = love.graphics.newImage("gfx/car.png")
	i_car_xo = i_car:getWidth() / 2
	i_car_yo = 0
	i_car_x = w / 2
	i_car_y = h - i_car:getHeight() * 2
	b_car = love.physics.newBody(world, i_car_x, i_car_y + i_car:getHeight() / 2, 0, 0)
	car_w = i_car:getWidth()
	car_h = i_car:getHeight()
	s_car = love.physics.newPolygonShape(b_car,
		-car_w/2, -car_h / 2 + 25,
		-car_w/2 + 3, -car_h / 2 + 5,
		-car_w/2 + 17, -car_h / 2,
		car_w/2 - 17, -car_h / 2,
		car_w/2 - 3, -car_h / 2 + 5,
		car_w / 2, -car_h / 2 + 25,
		car_w / 2, -car_h / 2 + car_h,
		-car_w / 2, -car_h / 2 + car_h
	)

	i_ldoor = love.graphics.newImage("gfx/ldoor.png")
	ldoor_x = i_car_x - 29 - i_ldoor:getWidth()
	ldoor_y = i_car_y + 54
	i_ldoor_xo = 0
	i_ldoor_yo = 0
	b_ldoor = love.physics.newBody(world, ldoor_x, ldoor_y)
	s_ldoor = love.physics.newRectangleShape(b_ldoor, i_car:getWidth() / 4, i_ldoor:getHeight()/2, i_car:getWidth() / 2, i_ldoor:getHeight())
	s_ldoor:setRestitution(1)
	b_ldoor:setAngle(1)


	i_rdoor = love.graphics.newImage("gfx/rdoor.png")
	rdoor_x = i_car_x + 29
	rdoor_y = i_car_y + 54
	i_rdoor_xo = 0
	i_rdoor_yo = 0
	b_rdoor = love.physics.newBody(world, rdoor_x, rdoor_y)
	s_rdoor = love.physics.newRectangleShape(b_rdoor, i_rdoor:getWidth() - i_car:getWidth() / 4, i_rdoor:getHeight()/2, i_car:getWidth() / 2, i_rdoor:getHeight())
	s_rdoor:setRestitution(1)

	local img = love.graphics.newImage("gfx/bike1_anim.png")
	a_bike = newAnimation(img, 43, 113, .2, 0)

	s_door = {}
	for i=1, 4 do
		s_door[i] = love.audio.newSource('snd/' .. i .. '.mp3', 'static')
	end

	way = 0
	speed = 3 -- m/s
	omega = 10

	door = true -- true: left open; false: right open
	angle = 1

	bodies = {}
end

function love.update(dt)
	world:update(dt)
	a_bike:update(dt)
	for k, v in ipairs(bodies) do
		if v.body:getX() < -10 or v.body:getX() > w + 10 or v.body:getY() < -10 or v.body:getY() > h + 20 then
			table.remove(bodies, k)
		end
	end
	if love.keyboard.isDown('up') then
		speed = speed + 20 * dt
		for k, v in ipairs(bodies) do
			v.body:applyImpulse(0, 20 * dt * v.body:getMass(), 0, 0)
		end
	elseif love.keyboard.isDown('down') then
		speed = speed - 20 * dt
		if speed < 0 then
			speed = 0
		else
			for k, v in ipairs(bodies) do
				v.body:applyImpulse(0, -20 * dt * v.body:getMass(), 0, 0)
			end
		end
	end
	way = ( way + speed * dt * u ) % ( 2 * u )
	if door_changing then
		if door_changing == 0 then
			angle = angle - omega * dt
			if angle <= 0 then
				angle = -angle
				door_changing = 1
				door = not door
				if not snd_played then
					love.audio.play(s_door[ math.random(2) + (door and 2 or 0) ])
				end
				snd_played = false
			elseif angle - 2 * dt * omega < 0 and not snd_played then
				snd_played = true
				love.audio.play(s_door[ math.random(2) + (door and 2 or 0) ])
			end
		elseif door_changing == 1 then
			angle = angle + 4 * omega * dt
			if angle >= 1 then
				angle = 1
				door_changing = nil
			end
		end
		if door then
			b_ldoor:setAngle(angle)
			b_rdoor:setAngle(0)
			b_rdoor:setPosition(rdoor_x, rdoor_y)
		else
			b_ldoor:setAngle(0)
			b_rdoor:setPosition(rdoor_x + i_rdoor:getWidth() * ( 1 - math.cos(angle)), rdoor_y + i_rdoor:getWidth() * math.sin(angle))
			b_rdoor:setAngle(-angle)
		end
	end
end

function love.draw()
	-- road
	love.graphics.setColor(100, 100, 100)
	love.graphics.rectangle("fill", 30, 0, w - 60, h)
	love.graphics.setColor(200, 200, 200)
	love.graphics.rectangle("fill", 30, 0, 5, h)
	love.graphics.rectangle("fill", w - 35, 0, 5, h)

	s = way - 2 * u
	while s < h do
		love.graphics.rectangle("fill", w/2 - 3, s, 6, u)
		s = s + 2 * u
	end

	love.graphics.draw(i_car, i_car_x, i_car_y, 0, 1, 1, i_car_xo, i_car_yo)
	if door then
		love.graphics.draw(i_rdoor, rdoor_x, rdoor_y, 0, 1, 1, i_rdoor_xo, i_rdoor_yo)
		love.graphics.draw(i_ldoor, ldoor_x, ldoor_y, angle, 1, 1, i_ldoor_xo, i_ldoor_yo)
	else
		love.graphics.draw(i_rdoor, rdoor_x + i_rdoor:getWidth() * ( 1 - math.cos(angle)), rdoor_y + i_rdoor:getWidth() * math.sin(angle), -angle, 1, 1, i_rdoor_xo, i_rdoor_yo)
		love.graphics.draw(i_ldoor, ldoor_x, ldoor_y, mousepressed and 1 or 0, 1, 1, i_ldoor_xo, i_ldoor_yo)
	end

	love.graphics.setColor(255, 100, 100)
	for k, v in ipairs(bodies) do
--		love.graphics.circle("fill",  v.body:getX(), v.body:getY(), v.shape:getRadius(), 10)
		v.draw:draw(v.body:getX() - 21, v.body:getY() - 56)
	end

	-- debug
	--[[
	love.graphics.setColor(200, 200, 200)
	love.graphics.polygon("line", s_car:getPoints())
	love.graphics.polygon("line", s_ldoor:getPoints())
	love.graphics.polygon("line", s_rdoor:getPoints())
	--]]
end

function love.mousepressed(x, y, btn)
	if btn == 'l' then door_change() end
end

function love.keypressed(key, unicode)
	if key == ' ' then
		door_change()
	elseif key == 'n' then
		add_stuff()
	elseif key == 'escape' then
		os.exit(0)
	end
end

function door_change()
	door_changing = 0
end

function add_stuff()
	--[[
	local r = 3
	local i = r
	while i + 3 < w do
		local b = love.physics.newBody(world, i, 20, 10, 0)
		local s = love.physics.newCircleShape(b, 0, 0, r)
		b:applyImpulse(0, (speed + 4) * b:getMass(), 0, 0)
		table.insert(bodies, {body=b, shape=s})
		i = i + 2 * r
	end
	--]]
	local b = love.physics.newBody(world, math.random(2) == 1 and u or w - u, 70, 10)
	b:applyImpulse(0, (speed + 4) * b:getMass(), 0, 0)
	local s = love.physics.newRectangleShape(b, 21, 113)
	local i = a_bike
	table.insert(bodies, {body=b, shape=s, draw=i})
end
